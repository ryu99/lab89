FROM golang:latest
MAINTAINER Roman Yurchevsky <romanyu1999@gmail.com>

WORKDIR /server/
COPY . .

EXPOSE 8080

RUN go install

CMD ["server"]