create table cities_population
(
   id int not null auto_increment primary key,
   name varchar(64) not null,
   population int not null
);

insert into cities_population(name, population) values ('Grodno', 400000),
       ('Minsk', 2000000),
       ('Gomel', 550000),
       ('Brest', 350000);