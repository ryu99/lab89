package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"html/template"
	"net/http"
)

type CityPopulation struct {
	ID         int64
	City       string
	Population int64
}

func main() {
	router := http.NewServeMux()
	router.HandleFunc("/population", CitiesPopulationHandler)
	server := http.Server{
		Addr:    ":8080",
		Handler: router,
	}
	server.ListenAndServe()
}

func CitiesPopulationHandler(w http.ResponseWriter, r *http.Request) {
	db, err := sql.Open("mysql", "yurchevsky:lab89@tcp(db:3306)/population")

	if err != nil {
		panic(err)
	}
	defer db.Close()

	cps := make([]*CityPopulation, 0)

	rows, err := db.Query("select * from cities_population")
	if err != nil {
		fmt.Fprint(w, err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		cp := CityPopulation{}
		rows.Scan(
			&cp.ID,
			&cp.City,
			&cp.Population,
		)
		cps = append(cps, &cp)
	}

	tmpl, _ := template.ParseFiles("index.html")
	tmpl.Execute(w, cps)
}
